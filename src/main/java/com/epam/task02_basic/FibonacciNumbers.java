package com.epam.task02_basic;

import java.util.Scanner;

public class FibonacciNumbers {
    public static long getFibonacciOf(long n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return getFibonacciOf(n - 2) + getFibonacciOf(n - 1);
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        long biggestOddNumber = 0, biggestEvenNumber = 0;
        while (true) {
            System.out.println("Enter N not more than 45  :");
            long n = scanner.nextLong();
            int lastTypeNumber = 1;
            double percentOdd, percentEven, counterOdd = 0, counterEven = 0;
            if (n >= 0) {
                long fibo = getFibonacciOf(n);
                System.out.println("F(" + n + ") = " + fibo);
                for (lastTypeNumber = (int) n; lastTypeNumber > 0; lastTypeNumber--) {
                    if (getFibonacciOf(lastTypeNumber) % 2 == 1) {
                        counterOdd++;
                    }
                    if (getFibonacciOf(lastTypeNumber) % 2 == 0) {
                        counterEven++;
                    }
                }
                if (fibo % 2 == 1) {
                    biggestOddNumber = fibo;
                    if (n > 3)
                        while (fibo % 2 == 1) {
                            fibo = getFibonacciOf(n -= 1);
                        }
                }
                if (fibo % 2 == 0) {
                    biggestEvenNumber = fibo;
                    if (n > 3)
                        while (fibo % 2 == 0) {
                            fibo = getFibonacciOf(n -= 1);
                        }
                }
                percentOdd = counterOdd / (counterOdd + counterEven) * 100;
                percentEven = counterEven / (counterOdd + counterEven) * 100;
                System.out.println("biggest odd number is " + biggestOddNumber + " counterOdd " + counterOdd);
                System.out.println("biggest even number is " + biggestEvenNumber + " counterEven " + counterEven);
                System.out.println("percent meetings Odd = " + percentOdd);
                System.out.println("percent meetings Event = " + percentEven);
            } else {
                break;

            }
        }
    }

}


