package com.epam.task02_basic;  //Home task N2

import java.util.Scanner;   //For insert numbers in program

/**
 * User enter the interval from.. to..
 * Program prints odd numbers from start to the end of interval and even from end to start
 * Program prints the sum of odd and even numbers
 * User enter the size of set (N) by Fibonacci numbers
 * Program build Fibonacci numbers and show the biggest odd number and the biggest even number by sequence
 * Program prints percentage of odd and even Fibonacci numbers
 */
public class App {

    //The method build and return Fibonacci numbers
    public static long getFibonacciOf(long n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else {
            return getFibonacciOf(n - 2) + getFibonacciOf(n - 1);
        }
    }

    public static void main(String[] args) {
        Scanner a = new Scanner(System.in);     //make object Class Scanner - a
        Scanner b = new Scanner(System.in);     //make object Class Scanner - b
        Scanner numberFibonacci = new Scanner(System.in);    //make object Class Scanner - number Fibonacci
        int numBeginInterval = 0, numEndInterval = 0, numberFibo = 0;   //determined variables
        int definerOddEvenNum = 0, numSequenCheck = 0;  //determined variables
        int sumOddNumbers = 0, summEvenNumbers = 0;     //determined variables

        //return true if entered  quite number and write it down in variables the width of the interval
        boolean boolSequenCheck;
        System.out.println("enter the beginning of the interval: ");
        if (a.hasNextInt()) {
            System.out.println("enter the end of the interval: ");
        } else System.out.println("You're wrong");
        if (b.hasNextInt()) {
        } else System.out.println("You're wrong");

        numBeginInterval = a.nextInt();
        numEndInterval = b.nextInt();

        // determines the correctness of the interval
        if (numBeginInterval < numEndInterval) {
            System.out.print("Program determines odd numbers from "
                    + numBeginInterval + " to " + numEndInterval + " then prints ");

            //Program prints odd numbers from start to the end of interval
            for (numSequenCheck = numBeginInterval; numSequenCheck <= numEndInterval; numSequenCheck++) {

                //consider only odd numbers
                if ((numSequenCheck % 2 == 1) || ((numSequenCheck * (-1)) % 2 == 1)) {
                    boolSequenCheck = true;
                    definerOddEvenNum = 3;
                    while (definerOddEvenNum < numSequenCheck / 2 && boolSequenCheck) {
                        if (numSequenCheck % definerOddEvenNum == 0) {
                            boolSequenCheck = false;
                        } else {
                            definerOddEvenNum = definerOddEvenNum + 2;
                        }
                    }
                    if (boolSequenCheck = true) {
                        System.out.print(numSequenCheck + ", ");    //print odd numbers
                        sumOddNumbers += numSequenCheck;    //sum the odd numbers in consequence
                    }
                } else {
                    summEvenNumbers += numSequenCheck;  //sum the even numbers in consequence
                }
            }
        } else {
            System.out.println("You're wrong");
        }
        System.out.println("");

        // determines the correctness of the interval
        if (numBeginInterval < numEndInterval) {
            System.out.print("Program determines odd numbers from " +
                    numEndInterval + " to " + numBeginInterval + " then prints ");

            //Program prints even numbers from end to the start of interval
            for (numSequenCheck = numEndInterval; numSequenCheck >= numBeginInterval; numSequenCheck--) {

                //consider only even numbers
                if ((numSequenCheck % 2 == 0) || ((numSequenCheck * (-1)) % 2 == 0)) {
                    boolSequenCheck = true;
                    definerOddEvenNum = 3;
                    while (definerOddEvenNum < numSequenCheck / 2 && boolSequenCheck) {
                        if (numSequenCheck % definerOddEvenNum == 1) {
                            boolSequenCheck = false;
                        } else definerOddEvenNum = definerOddEvenNum + 2;
                    }
                    if (boolSequenCheck = true) {
                        System.out.print(numSequenCheck + ", ");    //print even numbers
                    }
                }
            }
        } else {
            System.out.println("You're wrong");
        }
        System.out.println("");
        System.out.println("Summ odd numbers is " + sumOddNumbers); //print sum odd numbers
        System.out.println("Summ even numbers is " + summEvenNumbers);  //print sum even numbers


        //* Program doing Fibonacci number operations*//

        // Inserting  Fibonacci number in cycle

        while (true) {
            System.out.println("Enter number by Fibonacci that not more than 45  :");
            numberFibo = numberFibonacci.nextInt();
            long biggestOddNumber = 0, biggestEvenNumber = 0;   //determined variables
            // determines the correctness of the Fibonacci number

            if (numberFibo >= 0) {

                //Program print Fibonacci number
                int lastFiboTypeNumber = 1;     //determined variables in cycle
                double percentOdd = 0, percentEven = 0, counterOdd = 0, counterEven = 0; //determined variables in cycle
                long fibo = getFibonacciOf(numberFibo);
                System.out.println("F(" + numberFibo + ") = " + fibo);

                //Program calculating the numbers of odd and even meetings
                for (lastFiboTypeNumber = numberFibo; lastFiboTypeNumber > 0; lastFiboTypeNumber--) {
                    if (getFibonacciOf(lastFiboTypeNumber) % 2 == 1) {
                        counterOdd++;
                    }
                    if (getFibonacciOf(lastFiboTypeNumber) % 2 == 0) {
                        counterEven++;
                    }
                }

                //Program search the biggest odd and even numbers
                if (fibo % 2 == 1) {
                    biggestOddNumber = fibo;
                    if (numberFibo > 3) {
                        while (fibo % 2 == 1) {
                            if (numberFibo > 0) {
                                fibo = getFibonacciOf((numberFibo -= 1));
                            }
                        }
                        biggestEvenNumber = fibo;
                    }
                } else {
                    if (fibo % 2 == 0) {
                        biggestEvenNumber = fibo;
                        if (numberFibo > 2) {
                            while (fibo % 2 == 0) {
                                fibo = getFibonacciOf(numberFibo -= 1);
                            }
                            biggestOddNumber = fibo;
                        }
                    }
                }

                // Calculates percentage of odd and even Fibonacci numbers
                percentOdd = counterOdd / (counterOdd + counterEven) * 100;
                percentEven = counterEven / (counterOdd + counterEven) * 100;

                //Show calculated result Fibonacci number operations
                System.out.println("biggest odd number is " + biggestOddNumber
                        + " , amount Even numbers is " + counterOdd);
                System.out.println("biggest even number is " + biggestEvenNumber
                        + " , amount Even numbers is " + counterEven);
                System.out.println("percent meetings Odd = " + percentOdd);
                System.out.println("percent meetings Event = " + percentEven);

            } else break;
        }


    }
}
